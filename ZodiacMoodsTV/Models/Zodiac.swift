//
//  Zodiac.swift
//  ZodiacMoodsTV
//
//  Created by Ilker Baltaci on 12.02.19.
//  Copyright © 2019 Ilker Baltaci. All rights reserved.
//

import Foundation
import UIKit

public struct Zodiac {
        
    let name: String
    public let identifier: String
    let description: String
    let icon: UIImage?
    
    
}
