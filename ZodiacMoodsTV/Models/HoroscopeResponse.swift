//
//  HoroscopeResponse.swift
//  ZodiacMoodsTV
//
//  Created by Ilker Baltaci on 12.02.19.
//  Copyright © 2019 Ilker Baltaci. All rights reserved.
//

import Foundation

struct HoroscopeResponse: Codable {
    
    private static var currentDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    
    private static var staticDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter
    }()
    
    var formattedDate: String? {
        guard let date = self.date else { return nil }
        guard let currentDate = HoroscopeResponse.currentDateFormatter.date(from: date) else { return nil }
        return HoroscopeResponse.staticDateFormatter.string(from: currentDate)
    }
    
    let date: String?
    let sunsign: String?
    let horoscope: String?
    let meta: HoroscopeResponseMetaData?
}

struct HoroscopeResponseMetaData: Codable {
    let intensity: String?
    let keywords: String?
    let mood: String?
}
