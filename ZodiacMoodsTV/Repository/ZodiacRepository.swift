//
//  ZodiacRepository.swift
//  ZodiacMoodsTV
//
//  Created by Ilker Baltaci on 12.02.19.
//  Copyright © 2019 Ilker Baltaci. All rights reserved.
//

import Foundation


//
//  LoginRepository.swift
//  ExpertLeadTest
//
//  Created by Ilker Baltaci on 29.01.19.
//

import Foundation

struct ZodiacRepository {
    
    static let todayURL     = "http://theastrologer-api.herokuapp.com/api/horoscope/%@/today";
    static let yesterdayURL = "http://theastrologer-api.herokuapp.com/api/horoscope/%@/yesterday";
    static let tomorowURL   = "http://theastrologer-api.herokuapp.com/api/horoscope/%@/tomorrow";
    
    static var cacheRequestTime: Date?
    static let maximumCachTime = 60.0 * 1.0 //Cache for one hour
    
    static func horospopeURL(horospoce: String, day: SelecteDayEnum) -> String {
        switch day {
        case .today:
            return String(format: ZodiacRepository.todayURL, horospoce)
        case .yesterday:
            return String(format: ZodiacRepository.yesterdayURL, horospoce)
        case .tomorrow:
            return String(format: ZodiacRepository.tomorowURL, horospoce)
        }
    }
    static func fetchHorospope(zodiac: Zodiac, selectedDay: SelecteDayEnum, completion: @escaping (HoroscopeResponse?, SelecteDayEnum,_ success: Bool)->Void){
        let url = URL(string: ZodiacRepository.horospopeURL(horospoce: zodiac.identifier, day: selectedDay))!
        let session = URLSession.shared
        var request = URLRequest(url: url)
    
        if let lastRequestTime = ZodiacRepository.cacheRequestTime, lastRequestTime.timeIntervalSinceNow > -ZodiacRepository.maximumCachTime {
            debugPrint("Still new")
        } else {
            URLCache.shared.removeAllCachedResponses()
            cacheRequestTime = Date()
        }
        
        request.cachePolicy = .returnCacheDataElseLoad
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
    
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            DispatchQueue.main.async {
                guard
                    let httpResponse = response as? HTTPURLResponse,
                    let data = data,
                    error == nil else {
                        completion(nil,selectedDay,false)
                        return
                }
                
                guard let loginReponse = try? JSONDecoder().decode(HoroscopeResponse.self, from: data) else {
                    completion(nil,selectedDay,false)
                    return
                }
                
                completion(loginReponse, selectedDay, httpResponse.statusCode == 200)
            }
        })
        task.resume()
    }

}
