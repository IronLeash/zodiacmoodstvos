//
//  DetailsViewController.swift
//  ZodiacMoodsTV
//
//  Created by Ilker Baltaci on 12.02.19.
//  Copyright © 2019 Ilker Baltaci. All rights reserved.
//

import UIKit

protocol DetailsView {
    var  viewController: DetailsViewController { get }
    func showInfoOverlay()
    func hideInfoOverlay()
    func hideLoadingIndication()
    func showLoadingIndication()
    func hideViewElemens()
    func showViewElemens()
    func setupUI(horospoce: HoroscopeResponse, zodiac: Zodiac, selectedDay: SelecteDayEnum)
}


enum SelecteDayEnum {
    case today
    case tomorrow
    case yesterday
}

protocol  DetailsViewControllerDelegate: class {
    func detailsViewControllerShouldLoadHorospoce(sender: DetailsViewController, zodiac: Zodiac, selectedDay: SelecteDayEnum?)
}

class DetailsViewController: UIViewController {
    
    @IBOutlet var zodiacNameLabel: UILabel!
    @IBOutlet var loadingIndication: UIActivityIndicatorView!
    @IBOutlet var horospoceLabel: UILabel!
    @IBOutlet var previousDayButton: UIButton!
    @IBOutlet var nextDayButton: UIButton!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var zodiacImageView: UIImageView!
    @IBOutlet var overlayLabel: UILabel!
    
    
    private var selectedZodiac: Zodiac?
    private var selectedDay = SelecteDayEnum.today {
        didSet{ setupDayButtons() }
    }
    private var nextDay: SelecteDayEnum? {
        switch self.selectedDay {
        case SelecteDayEnum.today:
            return SelecteDayEnum.tomorrow
        case SelecteDayEnum.yesterday:
            return SelecteDayEnum.today
        case SelecteDayEnum.tomorrow:
            return nil
        }
    }
    
    private var previousDay: SelecteDayEnum? {
        switch self.selectedDay {
        case SelecteDayEnum.today:
            return SelecteDayEnum.yesterday
        case SelecteDayEnum.yesterday:
            return nil
        case SelecteDayEnum.tomorrow:
            return SelecteDayEnum.today
        }
    }
    
    fileprivate var uiElements: [UIView] = []
    
    weak var delegate: DetailsViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiElements = [zodiacNameLabel, horospoceLabel,dateLabel,previousDayButton,nextDayButton, zodiacImageView]
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func didTapPreviousDayButton(_ sender: UIButton) {
        delegate?.detailsViewControllerShouldLoadHorospoce(sender: self, zodiac: selectedZodiac! ,selectedDay: previousDay)
    }
    
    @IBAction func didTapNextDayButton(_ sender: UIButton) {
        delegate?.detailsViewControllerShouldLoadHorospoce(sender: self, zodiac: selectedZodiac!, selectedDay: nextDay)
    }
    
    fileprivate func setupDayButtons() {
        switch self.selectedDay {
        case SelecteDayEnum.today:
            nextDayButton.isEnabled = true
            previousDayButton.isEnabled = true
        case SelecteDayEnum.tomorrow:
            nextDayButton.isEnabled = false
            previousDayButton.isEnabled = true
        case SelecteDayEnum.yesterday:
            nextDayButton.isEnabled = true
            previousDayButton.isEnabled = false
        }
    }
}


extension DetailsViewController: DetailsView {
    var viewController: DetailsViewController { return self }
    
    func showInfoOverlay() { overlayLabel.isHidden = false }
    
    func hideInfoOverlay() { overlayLabel.isHidden = true }
    
    func hideLoadingIndication() { loadingIndication.isHidden = true }
    
    func showLoadingIndication() {
        loadingIndication.isHidden = false
        loadingIndication.startAnimating()
    }
    
    func hideViewElemens() {
        uiElements.forEach { (view) in
            view.isHidden = true
        }
    }
    
    func showViewElemens() {
        uiElements.forEach { (view) in
            view.isHidden = false
        }
    }
    
    func setupUI(horospoce: HoroscopeResponse, zodiac: Zodiac,selectedDay: SelecteDayEnum) {
        self.selectedZodiac = zodiac
        zodiacNameLabel.text = horospoce.sunsign?.capitalized
        horospoceLabel.text = horospoce.horoscope
        dateLabel.text = horospoce.formattedDate
        self.selectedDay = selectedDay
        zodiacImageView.image = zodiac.icon
    }
    
    
    
}



