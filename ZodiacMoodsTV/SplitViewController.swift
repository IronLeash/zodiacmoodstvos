//
//  ViewController.swift
//  ZodiacMoodsTV
//
//  Created by Ilker Baltaci on 12.02.19.
//  Copyright © 2019 Ilker Baltaci. All rights reserved.
//

import UIKit

class SplitViewController: UISplitViewController {

    
    var detailView: DetailsView? {
        return self.viewControllers.last as? DetailsViewController
    }
    var masterVC: MasterTableViewController? {
        return ((self.viewControllers.first) as? UINavigationController)?.topViewController as? MasterTableViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        masterVC?.delegate = self
        detailView?.viewController.delegate = self
        detailView?.hideViewElemens()
        detailView?.hideLoadingIndication()
        detailView?.showInfoOverlay()
    }
    
    fileprivate func loadHorospoceFor(zodiac: Zodiac, day: SelecteDayEnum) {
        detailView?.showLoadingIndication()
        detailView?.hideViewElemens()
        detailView?.hideInfoOverlay()
        ZodiacRepository.fetchHorospope(zodiac: zodiac, selectedDay: day) {[weak self] (response, selectedDay, success) in
            guard let self = self else {
                return
            }
            

            self.detailView?.hideLoadingIndication()
            guard let horospoceResponse = response else {
            self.detailView?.showInfoOverlay()
                return
            }
            self.detailView?.showViewElemens()
            self.detailView?.setupUI(horospoce: horospoceResponse, zodiac: zodiac, selectedDay: selectedDay)
        }
    }
}

extension SplitViewController: MasterTableViewControllerDelegate {
    func masterTableViewControllerDelegateDidSelect(sender: MasterTableViewController, zodiac: Zodiac) {
        loadHorospoceFor(zodiac: zodiac, day: SelecteDayEnum.today)
    }
}

extension SplitViewController: DetailsViewControllerDelegate {
    func detailsViewControllerShouldLoadHorospoce(sender: DetailsViewController, zodiac: Zodiac, selectedDay: SelecteDayEnum?) {
        guard let desiredDay = selectedDay else { return  }
        loadHorospoceFor(zodiac: zodiac, day: desiredDay)
        
    }
    
    
}

