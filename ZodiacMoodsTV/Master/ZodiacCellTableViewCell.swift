//
//  ZodiacCellTableViewCell.swift
//  ZodiacMoodsTV
//
//  Created by Ilker Baltaci on 12.02.19.
//  Copyright © 2019 Ilker Baltaci. All rights reserved.
//

import UIKit

class ZodiacCellTableViewCell: UITableViewCell {

    @IBOutlet var zodiacImageView: UIImageView!
    @IBOutlet var zodiacNameLabel: UILabel!
    @IBOutlet var zodiacDatesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        zodiacNameLabel.textColor = selected ? UIColor.black : UIColor.white
        zodiacDatesLabel.textColor = selected ? UIColor.black : UIColor.white
        zodiacImageView.tintColor = selected ? UIColor.black : UIColor.white
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        zodiacNameLabel.textColor = highlighted ? UIColor.black : UIColor.white
        zodiacDatesLabel.textColor = highlighted ? UIColor.black : UIColor.white
        zodiacImageView.tintColor = highlighted ? UIColor.black : UIColor.white
    }
    


}
