//
//  MasterTableViewController.swift
//  ZodiacMoodsTV
//
//  Created by Ilker Baltaci on 12.02.19.
//  Copyright © 2019 Ilker Baltaci. All rights reserved.
//

import UIKit


protocol MasterTableViewControllerDelegate: class {
    func masterTableViewControllerDelegateDidSelect(sender: MasterTableViewController, zodiac: Zodiac)
}

class MasterTableViewController: UITableViewController {

    
    weak var delegate: MasterTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int { return 1 }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return ZodiacDatasource.zodiacs.count }

    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { return 120 }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "zodiacZellIdentifier", for: indexPath) as! ZodiacCellTableViewCell
        let zodiac = ZodiacDatasource.zodiacs[indexPath.row]
        cell.zodiacNameLabel.text = zodiac.name
        cell.zodiacDatesLabel.text = zodiac.description
        cell.zodiacImageView.image = zodiac.icon
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.masterTableViewControllerDelegateDidSelect(sender: self, zodiac: ZodiacDatasource.zodiacs[indexPath.row])
    }


}
