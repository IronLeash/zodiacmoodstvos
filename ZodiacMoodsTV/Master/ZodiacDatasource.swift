//
//  ZodiacDatasource.swift
//  ZodiacMoodsTV
//
//  Created by Ilker Baltaci on 12.02.19.
//  Copyright © 2019 Ilker Baltaci. All rights reserved.
//

import Foundation
import UIKit
struct ZodiacDatasource {
    

    private static let aquarius = Zodiac(name: "Aquarius", identifier: "aquarius", description: "20.01 - 18.02", icon: UIImage(named: "aquarius"))
    private static let pisces = Zodiac(name: "Pisces", identifier: "pisces", description: "19.02 - 20.03", icon: UIImage(named: "pisces"))
    private static let aries = Zodiac(name: "Aries", identifier: "aries", description: "21.03 - 19.04", icon: UIImage(named: "aries"))
    private static let taurus = Zodiac(name: "Taurus", identifier: "taurus", description: "20.04 - 20.05", icon: UIImage(named: "taurus"))
    private static let gemini = Zodiac(name: "Gemini", identifier: "gemini", description: "21.05 - 20.06", icon: UIImage(named: "gemini"))
    private static let cancer = Zodiac(name: "Cancer", identifier: "cancer", description: "21.06 - 22.07", icon: UIImage(named: "cancer"))
    private static let leo = Zodiac(name: "Leo", identifier: "leo", description: "23.07 - 22.08", icon: UIImage(named: "leo"))
    private static let virgo = Zodiac(name: "Virgo", identifier: "virgo", description: "23.08 - 22.09", icon: UIImage(named: "virgo"))
    private static let libra = Zodiac(name: "Libra", identifier: "libra", description: "23.09 - 22.10", icon: UIImage(named: "scorpio"))
    private static let scorpio = Zodiac(name: "Scorpio", identifier: "scorpio", description: "23.10 - 21.11", icon: UIImage(named: "pisces"))
    private static let sagittarius = Zodiac(name: "Sagittarius", identifier: "sagittarius", description: "22.11 - 21.12", icon: UIImage(named: "sagittarius"))
    private static let capricorn = Zodiac(name: "Capricorn", identifier: "capricorn", description: "22.12 - 19.01", icon: UIImage(named: "capricorn"))

    public static let zodiacs = [ZodiacDatasource.aquarius,
                                 ZodiacDatasource.pisces,
                                 ZodiacDatasource.aries,
                                 ZodiacDatasource.taurus,
                                 ZodiacDatasource.gemini,
                                 ZodiacDatasource.cancer,
                                 ZodiacDatasource.leo,
                                 ZodiacDatasource.virgo,
                                 ZodiacDatasource.libra,
                                 ZodiacDatasource.scorpio,
                                 ZodiacDatasource.sagittarius,
                                 ZodiacDatasource.capricorn]
}
